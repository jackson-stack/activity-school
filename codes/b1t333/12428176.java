 
/*
 * SonarQube，开源软件质量管理工具。
 * 版权所有 (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube 是免费软件；您可以重新分发它和/或
 * 根据 GNU Lesser General Public 的条款修改它
 * 由自由软件基金会发布的许可证； 
 * 许可证的第 3 版，或（由您选择）任何更高版本。
 *
 * 分发 SonarQube 是为了希望它有用，
 * 但没有任何保证；甚至没有
 * 适销性或适用于特定目的的暗示保证。有关详细信息，请参阅 GNU
 * 宽松通用公共许可证。
 *
 * 您应该已经收到一份 GNU 宽松通用公共许可证的副本
 * 以及此程序；如果没有，请写信给自由软件基金会，
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA。
 */	
	public static void bubbleSort(int [] a, int n)
	{
		for(int i = 0; i < n - 1; i ++)
		{
			for(int j = 0; j < n - 1 - i; j++)
			{
				if(a[j] > a[j+1])
				{
					int temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
		}
	}
			
